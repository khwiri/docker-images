#!/bin/bash
set -e # exit when any commands fail

apt update --yes

apt install --yes \
  git \
  make \
  wget \
  build-essential \
  zlib1g-dev \
  libffi-dev \
  libbz2-dev \
  libreadline-dev \
  libssl-dev

rm -rf /var/lib/apt/lists/*

git clone -b "v$PYENV_VERSION" --single-branch https://github.com/pyenv/pyenv.git /root/.pyenv

cat << EOF >> /root/.bash_profile
export PYENV_ROOT="/root/.pyenv"
export PATH="\$PYENV_ROOT/bin:\$PATH"
if command -v pyenv 1>/dev/null 2>&1; then
  eval "\$(pyenv init -)"
fi
EOF
