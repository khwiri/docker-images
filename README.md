# Docker Images

This is nothing more than a small collection of docker images.

 # Images

All images are based on Debian 9.

## pyenv

This image comes with [pyenv](https://github.com/pyenv/pyenv) which lets you easily install and manage multiple python versions.

**Tags:** [pyenv:latest, pyenv:1.2.13](pyenv/Dockerfile)

```
$ docker pull registry.gitlab.com/khwiri/docker-images/pyenv:latest
```

## nvm image

This images comes with [nvm](https://github.com/nvm-sh/nvm) which lets you easily install and manage multiple node versions.

**Tags:** [nvm:latest, nvm:0.33.11](nvm/Dockerfile)

```
$ docker pull registry.gitlab.com/khwiri/docker-images/nvm:latest
```

## pyenv and nvm image

Why not both?

**Tags:** [pyenv-nvm:latest, pyenv-nvm:1.2.13-0.33.11](pyenv-nvm/Dockerfile)

```
$ docker pull registry.gitlab.com/khwiri/docker-images/pyenv-nvm:latest
```
