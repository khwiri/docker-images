#!/bin/bash
set -e # exit when any commands fail

apt update --yes && apt install --yes git curl
rm -rf /var/lib/apt/lists/*

curl -o- https://raw.githubusercontent.com/creationix/nvm/v$NVM_VERSION/install.sh | bash
